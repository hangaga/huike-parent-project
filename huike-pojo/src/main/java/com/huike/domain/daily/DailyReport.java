package com.huike.domain.daily;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DailyReport {
    private String date;            //今日日期
    private Integer dailyClue;      //今日线索数
    private Integer dailyBusiness;  //今日商机数
    private Integer dailyContract;  //今日合同数
    private Double dailySales;      //今日销售额
}
