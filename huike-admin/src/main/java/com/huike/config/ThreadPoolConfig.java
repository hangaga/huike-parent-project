package com.huike.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.annotation.PreDestroy;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Slf4j
@EnableAsync    //开启异步任务
@Configuration
public class ThreadPoolConfig {

    private ThreadPoolExecutor threadPoolExecutor = null;
    private ThreadPoolTaskExecutor threadPoolTaskExecutor = null;

    @Bean("jdkThreadPool")
    public ThreadPoolExecutor threadPoolExecutor() {
        log.info("初始化项目的线程池（原始）");
        threadPoolExecutor = new ThreadPoolExecutor(10, 20,
                5,
                TimeUnit.MINUTES,
                new ArrayBlockingQueue<>(10),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());
        return threadPoolExecutor;
    }

    @Bean("springThreadPool")
    public ThreadPoolTaskExecutor taskExecutor() {
        log.info("初始化项目的线程池（spring）");
        threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(10);
        threadPoolTaskExecutor.setMaxPoolSize(20);
        threadPoolTaskExecutor.setKeepAliveSeconds(300);
        threadPoolTaskExecutor.setThreadNamePrefix("Huike-Thread-Pool-");
        threadPoolTaskExecutor.setQueueCapacity(10);
        return threadPoolTaskExecutor;
    }

    @PreDestroy//用于标注Bean被销毁前需要执行的方法
    public void shotdown() {
        log.info("服务器关闭，关闭线程池，释放资源");
        if (threadPoolExecutor != null) {
            log.info("服务器关闭，关闭线程池，释放资源threadPoolExecutor");
            threadPoolExecutor.shutdown();
        }
        if (threadPoolTaskExecutor != null) {
            log.info("服务器关闭，关闭线程池，释放资源threadPoolTaskExecutor");
            threadPoolTaskExecutor.shutdown();
        }
    }

}
