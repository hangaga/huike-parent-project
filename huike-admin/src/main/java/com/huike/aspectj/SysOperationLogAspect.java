package com.huike.aspectj;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.huike.common.annotation.Log;
import com.huike.common.enums.BusinessStatus;
import com.huike.common.exception.CustomException;
import com.huike.domain.system.SysOperLog;
import com.huike.domain.system.SysUser;
import com.huike.domain.system.dto.LoginUser;
import com.huike.service.ISysOperLogService;
import com.huike.utils.ip.AddressUtils;
import com.huike.utils.ip.IpUtils;
import com.huike.web.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Slf4j
@Aspect
@Component
public class SysOperationLogAspect {

    @Autowired
    private TokenService tokenService;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private ISysOperLogService iSysOperLogService;

    /**
     * 正常
     * @param joinPoint
     * @param operatelog
     * @param result
     */
    @AfterReturning(value = "execution(* com.huike.controller.*.*.*(..)) && @annotation(operatelog)", returning = "result")
    private void afterReturning(JoinPoint joinPoint, Log operatelog, Object result){
        handleLog(joinPoint,operatelog,result,null);
    }

    /**
     * 异常
     * @param joinPoint
     * @param operatelog
     * @param throwable
     */
    @AfterThrowing(value = "execution(* com.huike.controller.*.*.*(..)) && @annotation(operatelog)", throwing = "throwable")
    private void afterThrowing(JoinPoint joinPoint, Log operatelog, Throwable throwable ){
        handleLog(joinPoint,operatelog,null,throwable);
    }


/*    *//**
     * 环绕
     * @param joinPoint
     * @param operatelog
     * @return
     *//*
    @Around("execution(* com.huike.controller.*.*.*(..)) && @annotation(operatelog)")
    public Object sysOperationLog(ProceedingJoinPoint joinPoint, Log operatelog){
        log.info("记录系统操作日志");
        Object result = null;
        try {
            result = joinPoint.proceed();
            //返回参数 jsonResult -> result
            handleLog(joinPoint, operatelog, result, null);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            //返回参数 jsonResult -> null
            handleLog(joinPoint, operatelog, null, throwable);
            throw new CustomException(throwable.getMessage());  //继续往上抛给 全局异常
        }
        return result;
    }*/

    //记录日志方法
    private void handleLog(JoinPoint joinPoint, Log operatelog, Object result, Throwable throwable) {
        SysOperLog sysOperLog = new SysOperLog();
        LoginUser loginUser = tokenService.getLoginUser(request);
        SysUser user = loginUser.getUser();
        //模块标题 title
        sysOperLog.setTitle(operatelog.title());
        //业务类型（0其他 1新增 2修改 3删除）businessType
        sysOperLog.setBusinessType(operatelog.businessType().ordinal());
        //方法名称 method
        sysOperLog.setMethod(joinPoint.getTarget().getClass().getName() + "." + joinPoint.getSignature().getName());
        //请求方式 requestMethod
        sysOperLog.setRequestMethod(request.getMethod());
        //操作类别（0其它 1后台用户 2手机端用户） operatorType
        sysOperLog.setOperatorType(operatelog.operatorType().ordinal());
        //操作人员 operName
        sysOperLog.setOperName(user.getUserName());
        //部门名称 deptName
        sysOperLog.setDeptName(user.getDept().getDeptName());
        //请求地址 operUrl
        sysOperLog.setOperUrl(request.getRequestURI());
        //操作地址 operIp
        sysOperLog.setOperIp(IpUtils.getIpAddr(request));
        //操作地点 operLocation
        sysOperLog.setOperLocation(AddressUtils.getRealAddressByIP(sysOperLog.getOperIp()));
        //请求参数 operParam
        sysOperLog.setOperParam(JSON.toJSONString(joinPoint.getArgs()).replace("[", "").replace("]", ""));
        //返回参数 jsonResult
        sysOperLog.setJsonResult(JSONObject.toJSONString(result));
        //状态（0=正常,1=异常）status
        sysOperLog.setStatus(throwable == null ? BusinessStatus.SUCCESS.ordinal() : BusinessStatus.FAIL.ordinal());
        //操作时间 operTime
        sysOperLog.setOperTime(new Date());
        //错误消息 errorMsg
        if (throwable != null) {    //异常
            sysOperLog.setErrorMsg(throwable.getMessage());
        }
        iSysOperLogService.insertOperlog(sysOperLog);
    }
}
