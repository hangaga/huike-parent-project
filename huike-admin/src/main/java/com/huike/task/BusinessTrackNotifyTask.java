package com.huike.task;

import com.huike.domain.business.TbBusiness;
import com.huike.domain.system.SysUser;
import com.huike.mapper.SysDictDataMapper;
import com.huike.mapper.TbAssignRecordMapper;
import com.huike.mapper.TbBusinessMapper;
import com.huike.utils.EmailUtils;
import com.huike.utils.file.FileUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.io.File;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class BusinessTrackNotifyTask {

    @Autowired
    private TbBusinessMapper tbBusinessMapper;

    @Autowired
    private TbAssignRecordMapper tbAssignRecordMapper;

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;

    private static final String BUSINESS_TRACK_NOTIFY = "business:notify:";

    @Autowired
    private EmailUtils emailUtils;

    @Value("${spring.mail.username}")
    private String from;

    @Autowired
    private SysDictDataMapper sysDictDataMapper;

    //@Scheduled(cron = "0 0/5 * * * ?")//间隔5分钟
    @Scheduled(cron = "0/15 * * * * ?")
    public void sendNotifyEmail() throws Exception {
        List<TbBusiness> tbBusinessList = tbBusinessMapper.selectTbBusinessByStatusAndNextTime("2", LocalDateTime.now(), LocalDateTime.now().plusHours(2));
        if (!CollectionUtils.isEmpty(tbBusinessList)) {
            for (TbBusiness tbBusiness : tbBusinessList) {
                SysUser sysUser = tbAssignRecordMapper.selectByAssignId(tbBusiness.getId(), "1");
                Object flag = redisTemplate.opsForValue().get(BUSINESS_TRACK_NOTIFY + tbBusiness.getId());
                if (ObjectUtils.isEmpty(flag)) {//缓存标识为空，没发过邮件
                    if (sysUser != null) {//发邮件
                        sendEmail(sysUser, tbBusiness);
                    }
                }
            }
        }
    }

    private void sendEmail(SysUser sysUser, TbBusiness tbBusiness) throws Exception {
        String text = handleMailText(sysUser, tbBusiness);
        emailUtils.sendMailWithoutAttachment(from, sysUser.getEmail(), "商机跟进提醒", text);
        redisTemplate.opsForValue().set(BUSINESS_TRACK_NOTIFY + tbBusiness.getId(), "-", 3, TimeUnit.HOURS);
        log.info("已发送商机跟进邮件，并存入缓存，{}", BUSINESS_TRACK_NOTIFY + tbBusiness.getId());
    }

    private String handleMailText(SysUser sysUser, TbBusiness tbBusiness) throws Exception {
        String file = this.getClass().getClassLoader().getResource("templates/BusinessNotifyTemplate.html").getFile();
        String text = FileUtils.readFileToString(new File(file), "UTF-8");
        //意向学科
        String subject = sysDictDataMapper.selectDictLabel("course_subject", tbBusiness.getSubject());
        return String.format(text, sysUser.getRealName(), tbBusiness.getId(), tbBusiness.getName(), tbBusiness.getPhone(), tbBusiness.getSex().equals("1") ? "男" : "女", subject);
    }
}
