package com.huike.task;

import com.huike.domain.clues.TbAssignRecord;
import com.huike.domain.clues.TbClue;
import com.huike.domain.system.SysUser;
import com.huike.mapper.TbAssignRecordMapper;
import com.huike.mapper.TbClueMapper;
import com.huike.utils.EmailUtils;
import com.huike.utils.file.FileUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.io.File;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class ClueTrackNotifyTask {

    @Autowired
    private TbClueMapper tbClueMapper;

    @Autowired
    private TbAssignRecordMapper tbAssignRecordMapper;

    @Autowired
    private EmailUtils emailUtils;

    @Value("${spring.mail.username}")
    private String from;

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;

    private static final String CLUE_TRACK_NOTIFY = "clue:notify:";

    @Scheduled(cron = "0 0/5 * * * *")////间隔5分钟
    public void sendNotifyEmail() throws Exception {
        //1、查询要跟进的线索
        List<TbClue> tbClues = tbClueMapper.findClueByStatusAndNextTime(TbClue.StatusType.FOLLOWING.getValue(), LocalDateTime.now(), LocalDateTime.now().plusHours(2));
        //2、线索人的信息
        if (!CollectionUtils.isEmpty(tbClues)) {
            for (TbClue tbClue : tbClues) {
                //**查询已发送标识
                Object flag = redisTemplate.opsForValue().get(CLUE_TRACK_NOTIFY + tbClue.getId());
                if (ObjectUtils.isEmpty(flag)) {  //标识为空
                    SysUser sysUser = tbAssignRecordMapper.selectByAssignId(tbClue.getId(), TbAssignRecord.RecordType.CLUES.getValue());
                    //3、发送邮件提醒
                    if (sysUser != null) {
                        sendEmail(sysUser, tbClue);
                    }
                }
            }
        }
    }

    private void sendEmail(SysUser sysUser, TbClue tbClue) throws Exception {
        //组装文本（模板）
        String text = handleMailText(sysUser, tbClue);
        emailUtils.sendMailWithoutAttachment(from, sysUser.getEmail(), "线索跟进提醒", text);
        //存储已发送标识
        redisTemplate.opsForValue().set(CLUE_TRACK_NOTIFY + tbClue.getId(), "-", 3, TimeUnit.HOURS);
        log.info("已发送邮件，存入缓存标识，{}", CLUE_TRACK_NOTIFY + tbClue.getId());
    }

    /*
        处理邮件模板
     */
    private String handleMailText(SysUser sysUser, TbClue tbClue) throws Exception {
        String file = this.getClass().getClassLoader().getResource("templates/ClueNotifyTemplate.html").getFile();
        String text = FileUtils.readFileToString(new File(file), "UTF-8");
        return String.format(text, sysUser.getRealName(), tbClue.getId(), tbClue.getName(), tbClue.getPhone(), tbClue.getSex().equals("1") ? "男" : "女");
    }
}
