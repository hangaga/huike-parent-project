package com.huike.task;

import com.huike.domain.system.SysUser;
import com.huike.mapper.SysUserMapper;
import com.huike.mapper.TbBusinessMapper;
import com.huike.mapper.TbClueMapper;
import com.huike.mapper.TbContractMapper;
import com.huike.utils.EmailUtils;
import com.huike.utils.file.FileUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

@Slf4j
@Component
public class MonthBasisTask {

    @Autowired
    private TbClueMapper tbClueMapper;

    @Autowired
    private TbBusinessMapper tbBusinessMapper;

    @Autowired
    private TbContractMapper tbContractMapper;

    @Autowired
    private EmailUtils emailUtils;

    @Value("${spring.mail.username}")
    private String from;

    @Autowired
    private SysUserMapper sysUserMapper;

    /*
        环比增长率 =（本期数－上期数）／上期数 ×100%
        指标：线索、商机、合同、销售金额（合同金额）
     */
    @Scheduled(cron = "0 0 23 L * ?")//每月最后一天晚上11点
    //@Scheduled(cron = "0/15 * * * * ?")//测试
    public void sendReportEmail() throws Exception {
        //本月
        LocalDate beginDate = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), 1);
        LocalDate endDate = YearMonth.now().atEndOfMonth();

        String begin = beginDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        String end = endDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        int allClues = tbClueMapper.countAllClues(begin, end);
        int allBusiness = tbBusinessMapper.countAllBusiness(begin, end);
        int allContracts = tbContractMapper.countAllContracts(begin, end);
        double sumSalesStatistics = tbContractMapper.sumAllSalesStatistics(begin, end);

        //上个月
        LocalDate _beginDate = LocalDate.of(LocalDate.now().minusMonths(1).getYear(), LocalDate.now().minusMonths(1).getMonthValue(), 1);
        LocalDate _endDate = YearMonth.now().minusMonths(1).atEndOfMonth();

        String _begin = _beginDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        String _end = _endDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        int _allClues = tbClueMapper.countAllClues(_begin, _end);
        int _allBusiness = tbBusinessMapper.countAllBusiness(_begin, _end);
        int _allContracts = tbContractMapper.countAllContracts(_begin, _end);
        double _sumSalesStatistics = tbContractMapper.sumAllSalesStatistics(_begin, _end);

        //线索、商机、合同、销售金额 环比率
        int cluesRate = (allClues - _allClues) * 100 / _allClues;
        int businessRate = (allBusiness - _allBusiness) * 100 / _allBusiness;
        int contractsRate = (allContracts - _allContracts) * 100 / _allContracts;
        double salesRate = (sumSalesStatistics - _sumSalesStatistics) * 100 / _sumSalesStatistics;

        //发送邮件  a.获取模板  b.填充数据  c.发送邮件
        String template = loadReportTemplate();
        SysUser sysUser = sysUserMapper.selectUserById(1L);
        String text = String.format(template, allClues, cluesRate, allBusiness, businessRate, allContracts, contractsRate, sumSalesStatistics, salesRate);
        emailUtils.sendMailWithoutAttachment(from, sysUser.getEmail(), "月度统计报表", text, null);

    }

    /*
        加载模板
     */
    private String loadReportTemplate() throws Exception {
        String file = this.getClass().getClassLoader().getResource("templates/MonthReport.html").getFile();
        return FileUtils.readFileToString(new File(file), "UTF-8");
    }

}
