package com.huike.task;

import com.huike.domain.daily.DailyReport;
import com.huike.domain.system.SysUser;
import com.huike.mapper.SysUserMapper;
import com.huike.mapper.TbBusinessMapper;
import com.huike.mapper.TbClueMapper;
import com.huike.mapper.TbContractMapper;
import com.huike.utils.EmailUtils;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.StringWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Component
public class MonthlyTask {

    @Autowired
    private Configuration configuration;

    @Autowired
    private TbClueMapper clueMapper;

    @Autowired
    private TbBusinessMapper businessMapper;

    @Autowired
    private TbContractMapper contractMapper;

    @Autowired
    private EmailUtils emailUtils;

    @Value("${spring.mail.username}")
    private String from;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Scheduled(cron = "0 0 23 L * ?")//每月月底
    //@Scheduled(cron = "0/5 * * * * ?")//测试
    public void sendMonthlyReportEmail() throws Exception {
        //1、日期 列表
//        LocalDate beginOfMonth = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), 1);//当月第一天
//        LocalDate endOfMonth = YearMonth.now().atEndOfMonth();//当月最后一天

        //方式二：
        LocalDate beginOfMonth = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth());//当月第一天
        LocalDate endOfMonth = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());//当月最后一天

        List<LocalDate> localDateList = beginOfMonth.datesUntil(endOfMonth.plusDays(1)).collect(Collectors.toList());//dateUntil包前不包后
        List<String> dateList = localDateList.stream().map(localDate -> localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))).collect(Collectors.toList());
        //准备：月初时间、月末时间
        LocalDateTime beginMonth = LocalDateTime.of(beginOfMonth, LocalTime.MIN);
        LocalDateTime endMonth = LocalDateTime.of(endOfMonth, LocalTime.MAX);
        //2、每日线索新增 列表
        List<DailyReport> clueList = clueMapper.dailyClue(beginMonth, endMonth);
        Map<String, Integer> clueMap = clueList.stream().collect(Collectors.toMap(DailyReport::getDate, DailyReport::getDailyClue));
        //3、每日商机新增 列表
        List<DailyReport> businessList = businessMapper.dailyBusiness(beginMonth, endMonth, "4");
        Map<String, Integer> businessMap = businessList.stream().collect(Collectors.toMap(DailyReport::getDate, DailyReport::getDailyBusiness));
        //4、每日合同新增 列表
        List<DailyReport> contractList = contractMapper.dailyContract(beginMonth, endMonth);
        Map<String, Integer> contractMap = contractList.stream().collect(Collectors.toMap(DailyReport::getDate, DailyReport::getDailyContract));
        //5、每日销售额 列表
        List<DailyReport> salesList = contractMapper.dailySales(beginMonth, endMonth);
        Map<String, Double> salesMap = salesList.stream().collect(Collectors.toMap(DailyReport::getDate, DailyReport::getDailySales));

        //数据模型
        Map<String, Object> dataModel = new HashMap<>();

        List<DailyReport> dailyReportList = dateList.stream().map(date -> {
            return DailyReport.builder()
                    .date(date)
                    .dailyClue(clueMap.get(date) == null ? 0 : clueMap.get(date))
                    .dailyBusiness(businessMap.get(date) == null ? 0 : businessMap.get(date))
                    .dailyContract(contractMap.get(date) == null ? 0 : contractMap.get(date))
                    .dailySales(salesMap.get(date) == null ? 0 : salesMap.get(date))
                    .build();
        }).collect(Collectors.toList());

        dataModel.put("dataList", dailyReportList);
        //获取模板
        Template template = configuration.getTemplate("MonthlyReport.ftl");
        //生成文本
        String text = FreeMarkerTemplateUtils.processTemplateIntoString(template, dataModel);
/*        //方法二：
        StringWriter stringWriter = new StringWriter();
        template.process(dataModel,stringWriter);
        String text = stringWriter.toString();*/

        SysUser sysUser = sysUserMapper.selectUserById(1L);
        String to = sysUser.getEmail();
        emailUtils.sendMailWithoutAttachment(from, to, "月度运营数据统计-详细信息", text, null);
        log.info("已发送月度运营数据统计-详细信息邮件到{}邮箱", to);
    }

}
