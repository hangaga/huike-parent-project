package com.huike.interceptor;

import com.huike.common.constant.Constants;
import com.huike.common.constant.HttpStatus;
import com.huike.domain.system.dto.LoginUser;
import com.huike.utils.StringUtils;
import com.huike.utils.redis.RedisCache;
import com.huike.web.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class LoginTokenInterceptor implements HandlerInterceptor {

    @Autowired
    private TokenService tokenService;

//    @Autowired
//    private RedisCache redisCache;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        LoginUser loginUser;
        try {
            loginUser = tokenService.getLoginUser(request);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        if(loginUser == null){
            return false;
        }
//        String uuid = loginUser.getToken();
//        String userKey = Constants.LOGIN_TOKEN_KEY + uuid;
//        Object cacheObject = redisCache.getCacheObject(userKey);
//        if(cacheObject == null){
//            return false;
//        }
        tokenService.refreshAndCacheToken(loginUser);
        return true;
    }
}
