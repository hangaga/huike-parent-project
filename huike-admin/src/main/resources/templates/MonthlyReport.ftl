<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>
<body>
<p>您好，超级管理员，以下是本月公司运营数据，请及时查收，数据有任何问题，请联系开发人员。</p>
<br>
<table>
    <tr>
        <th>日期</th>
        <th>线索新增</th>
        <th>商机新增</th>
        <th>合同新增</th>
        <th>销售额</th>
    </tr>

    <#list dataList as u>
        <tr>
            <td>${u.date}</td>
            <td>${u.dailyClue}</td>
            <td>${u.dailyBusiness}</td>
            <td>${u.dailyContract}</td>
            <td>${u.dailySales}</td>
        </tr>
    </#list>
</table>

</body>

<style>
    p{
        text-align: center;
        font-size: 20px;
    }
    table{
        margin-left: auto;
        margin-right: auto;
        width: 80%;
    }
    tr,td,th {
        text-align: center;
        font-size: medium;
        border-collapse: collapse;
        width: auto;
    }
    th{
        background: #F9C46B;
        height: 80px;
    }
    td{
        background: #E3E3E3;
        height: 60px;
    }

</style>
</html>