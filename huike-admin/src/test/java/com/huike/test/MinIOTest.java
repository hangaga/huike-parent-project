package com.huike.test;

import io.minio.*;
import org.junit.jupiter.api.Test;
import org.springframework.util.MimeTypeUtils;

import java.io.File;
import java.io.FileInputStream;

public class MinIOTest {

    @Test
    public void testUpload() {
        try {
            //1、创建一个 minioClient 对象
            MinioClient minioClient =
                    MinioClient.builder()
                            .endpoint("http://127.0.0.1:9000")
                            .credentials("qTmLiOLbfWhD29cbvqoo", "6Zr7XWVjr4Mm5W34dYb8nCEg5V3FVVwVENYhgEUV")
                            .build();

            boolean found = minioClient.bucketExists(BucketExistsArgs.builder().bucket("huike").build());
            if (!found) {
                minioClient.makeBucket(MakeBucketArgs.builder().bucket("huike").build());
            }
            //上传文件到指定bucket， => 本地问价
            minioClient.uploadObject(
                    UploadObjectArgs.builder()
                            .bucket("huike")
                            .object("1.jpg")
                            .filename("E:\\soft\\minio\\11.jpg")
                            .build());
        } catch (Exception e) {
            System.out.println("Error occurred: " + e);
        }
    }


    @Test
    public void testUpload2() {
        try {
            MinioClient minioClient =
                    MinioClient.builder()
                            .endpoint("http://127.0.0.1:9000")
                            .credentials("qTmLiOLbfWhD29cbvqoo", "6Zr7XWVjr4Mm5W34dYb8nCEg5V3FVVwVENYhgEUV")
                            .build();

            boolean found = minioClient.bucketExists(BucketExistsArgs.builder().bucket("crm").build());
            if (!found) {
                minioClient.makeBucket(MakeBucketArgs.builder().bucket("crm").build());
            }

            File file = new File("E:\\soft\\minio\\11.jpg");
            // 上传文件到指定bucket，- 已知大小的流
            minioClient.putObject(
                    PutObjectArgs.builder()
                            .bucket("huike")
                            .object("22.jpg")
                            .stream(new FileInputStream(file), file.length(), -1)
                            .contentType(MimeTypeUtils.IMAGE_JPEG_VALUE)
                            .build());
        } catch (Exception e) {
            System.out.println("Error occurred: " + e);
        }
    }

}