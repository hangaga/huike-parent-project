package com.huike.test;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DateListTest {

    @Test
    public void dateListTest(){
        //拿到当月第一天
        LocalDate beginOfMonth = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), 1);
        System.out.println(beginOfMonth);
        //拿到当月最后一天
        LocalDate endOfMonth = YearMonth.now().atEndOfMonth();
        System.out.println(endOfMonth);
        //收集、解析
        List<LocalDate> localDateList = beginOfMonth.datesUntil(endOfMonth.plusDays(1)).collect(Collectors.toList());
        List<String> list = localDateList.stream().map(localDate -> localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))).collect(Collectors.toList());
        System.out.println(list);

    }
}
