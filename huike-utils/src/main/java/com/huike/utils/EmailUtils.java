package com.huike.utils;

import com.huike.common.constant.MessageConstants;
import com.huike.common.exception.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.internet.MimeMessage;

public class EmailUtils {

    @Autowired
    private JavaMailSender javaMailSender;

    public EmailUtils(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void sendMailWithoutAttachment(String from, String to, String subject, String text, String... cc) throws Exception {
        //1、发件人为空 抛异常
        if (StringUtils.isEmpty(from)) {
            throw new CustomException(MessageConstants.EMAIL_FROM_NOT_NULL);
        }
        //2、收件人为空，抛异常
        if (StringUtils.isEmpty(to)) {
            throw new CustomException(MessageConstants.EMAIL_TO_NOT_NULL);
        }
        //3、组装邮件
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
        mimeMessageHelper.setFrom(from);
        mimeMessageHelper.setTo(to);
        mimeMessageHelper.setSubject(subject);
        mimeMessageHelper.setText(text, true);
        if (cc != null && cc.length > 0) {
            mimeMessageHelper.setCc(cc);
        }
        //4、发送邮件
        javaMailSender.send(mimeMessage);
    }


}
