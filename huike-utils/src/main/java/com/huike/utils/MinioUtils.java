package com.huike.utils;

import com.huike.common.config.MinioConfig;
import com.huike.utils.uuid.UUID;
import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MinioUtils {

    private MinioConfig minioConfig;

    public MinioUtils(MinioConfig minioConfig) {
        this.minioConfig = minioConfig;
    }

    public String upload(MultipartFile file) throws Exception {
        //1、客户端对象
        MinioClient minioClient =
                MinioClient.builder()
                        .endpoint("http://" + minioConfig.getEndpoint() + ":" + minioConfig.getPort())
                        .credentials(minioConfig.getAccessKey(), minioConfig.getSecretKey())
                        .build();
        //2、判断bucket是否存在，不存在则创建
        boolean found = minioClient.bucketExists(BucketExistsArgs.builder().bucket(minioConfig.getBucketName()).build());
        if (!found) {
            minioClient.makeBucket(MakeBucketArgs.builder().bucket(minioConfig.getBucketName()).build());
        }
        //3、文件名 eg：2023/6/xxxx.jpg
        String extname = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")); //后缀
        String objectName = UUID.randomUUID().toString() + extname;

        //根据月份分类（文件夹）
        String folderName = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/"));
        String fileName = folderName + objectName;

        //4、上传文件到指定bucket，- 已知大小的流
        minioClient.putObject(
                PutObjectArgs.builder()
                        .bucket(minioConfig.getBucketName())
                        .object(fileName)
                        .stream(file.getInputStream(), file.getSize(), -1)
                        .contentType(file.getContentType())
                        .build());
        //5、返回url
        return "/" + minioConfig.getBucketName() + "/" + fileName;
    }
}
